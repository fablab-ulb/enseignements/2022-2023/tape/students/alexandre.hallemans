# Qui suis-je ?



# Carnet de bord

## Lundi 2022-09-26

Pendant ce cours, nous avons discuté longuement des principes de base de l'open-source, ainsi que de différents exemples de programmes open-source : GIMP, InkScape, Libre/Open Office, ...

Nous avons ensuite discuté du projet de cette année : étudier les aspects techniques et physiques du [FoldScope](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0098781), un microscope optique en papier inventé par Manu Prakash et Jim Cybulski, pendant la thèse de doctorat de ce dernier. Ce microscope est très peu coûteux a fabriquer (\< 1$) et il est possible de l'expédier dans une enveloppe, ce qui permet de le transporter partout et de pouvoir l'expédier par exemple dans des pays en développement sans risque de le casser.

Nous avons ensuite eu, [Nasra](https://gitlab.com/fablab-ulb/enseignements/2022-2023/tape/students/nasra.daher) et moi, un exemplaire du Foldscope à construire -- ou plutôt à désassembler puis reconstruire pour notre part. Nous avons ensuite essayé d'observer une tique avec le dispositif, en vain. Difficultés à faire la mise au point ? Mauvais alignement ? Éclairage insuffisant ? À creuser à la prochaine séance... 

3 aspects techniques intéressants


### Lundi 2022-10-03

3 applications en détail (quoi, pourquoi, conclusions, méthodes, résultats)

